package flush.util;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

public class HbUtil {

	private static final SessionFactory sessionFactory = buildSessionFactory();
	private static ServiceRegistry registry;

	private static SessionFactory buildSessionFactory() {
		HbUtil.registry = new StandardServiceRegistryBuilder().configure().build();

		Metadata metaData = new MetadataSources(HbUtil.registry).getMetadataBuilder().build();

		return metaData.getSessionFactoryBuilder().build();
	}

	public static SessionFactory getSessionFactory() {
		return HbUtil.sessionFactory;
	}

	public static void shutdown() {
		HbUtil.sessionFactory.close();
	}

}
