package flush.pojos;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/*
 * @Entity is the only mandatory annotation.
 * From @Entity annotation only hibernate connect/map this class to the Table in Database
 */
@Entity

/*
 * @Table annotation is optional
 * If our class name and table name is same then there is no need to specify @Table annotation
 * If our class name and table name is different then only we have to specify @Table (name = <table_name>) annotation
 */
@Table(name = "customers")
/*
 * Is table name case sensitive?
 * - If Mysql is on linux platform then YES
 * - If mysql is on windows platform then NO
 */
public class Customer {
	
	/*
	 * There is no need to provide column name explicitly if name of column and name of data member is same.
	 * As hibernate by default use name of data member as name of column 
	 */
	
	/*
	 * In fact there is no need to provide @Column annotation on data members
	 * Hibernate by default took data member as column
	 * 
	 */
	
	/*
	 * What if we want any data member which should not connect/map to column of table, 
	 *  as hibernate by default connect/map data member to column of table ?
	 * Then we have to specify @Transient annotation explicitly which tells hibernate that
	 *   do not connect/map this data member to column of table.
	 */
	
	@Column(name = "id")
	
	/*
	 * @Id is Mandatory annotation 
	 */
	@Id
	private int id;
	
	@Column(name = "name")
	private String name;
	
	@Column
	private String password;
	
	private String mobile;
	private String address;
	
	/*
	 * If our column have unique constraint 
	 *  then it is optional but recommended to provide unique = true parameter in @Column annotation
	 */
	@Column(unique = true)
	private String email;
	private Date birth;
	
	@Transient		// Do not map this field to db column
	private Integer age;

	public Customer() {
	}

	public Customer(int id, String name, String password, String mobile, String address, String email, Date birth) {
		this.id = id;
		this.name = name;
		this.password = password;
		this.mobile = mobile;
		this.address = address;
		this.email = email;
		this.birth = birth;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", password=" + password + ", mobile=" + mobile + ", address="
				+ address + ", email=" + email + ", birth=" + birth + ", age=" + age + "]";
	}
}
