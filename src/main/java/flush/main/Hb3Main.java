package flush.main;

import java.sql.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import flush.pojos.Customer;
import flush.util.HbUtil;

public class Hb3Main {

	public static void main(String[] args) {
		SessionFactory sessionFactory = HbUtil.getSessionFactory();
		Session session = null;
		
		Transaction tx = null;
		try{
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			
			Customer cust = new Customer(10, "Shivam Palaskar 4", "shivamp@@123", "8482923285", "Pune", "shivamp5@cdac.in", new Date(1994, 9, 26));
			
			/*
			 * No SQL Query is executed on database after session.save() method.
			 */
			session.save(cust);
			
			/*
			 * Hibernate create query after session.flush() method.
			 * The Query is executed on temporary table created for transaction after session.flush() method.
			 * Customer data inserted in temporary table here.
			 */
			session.flush();
			
			/*
			 * If we try to get customer data having id 10 from table within this transaction then we will able to get data
			 *  as data is being fetched from temporary table.
			 * But if we access mysql via shell then there will be no customer data having id 10. 
			 */
			System.out.println(session.get(Customer.class, 10));
			
			/*
			 * If there is no exception  or error after session.flush() the SQL Query is executed on actual table after transaction commit.
			 * Customer data inserted in actual table here.
			 */
			tx.commit();
			
		} catch (Exception e) {
			tx.rollback();
			e.printStackTrace();
		}
		session.close();

	}

}